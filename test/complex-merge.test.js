const test = require('tape')
const Reduce = require('../')

const Strategy = require('@tangle/strategy')
const OverwriteStrategy = require('@tangle/overwrite')
const SimpleSetStrategy = require('@tangle/simple-set')

const strategy = new Strategy({
  title: OverwriteStrategy(),
  location: OverwriteStrategy(),
  attendees: SimpleSetStrategy(),
  time: OverwriteStrategy({ type: 'number' })
})

test('reduce (merge)', t => {
  // graph TD
  //
  //   A-->B-->C-->F
  //       B-->D-->F
  //       B-->E-->F

  const A = {
    key: 'A',
    data: { title: { set: 'bbq' } },
    previous: null
  }

  const B = {
    key: 'B',
    data: { title: { set: 'Mix\'s Party' } },
    previous: ['A']
  }

  const C = {
    key: 'C',
    data: { location: { set: 'Miramar, NZ' } },
    previous: ['A']
  }

  const D = {
    key: 'D',
    data: { time: { set: 4 } },
    previous: ['B']
  }

  const E = {
    key: 'E',
    data: { title: { set: 'Colin\'s Party' } },
    previous: ['B']
  }

  const F = {
    key: 'F',
    data: {},
    previous: ['C', 'D', 'E']
  }

  t.deepEqual(
    new Reduce(strategy, { nodes: [A, B, C, D, E, F] }).state,
    {
      F: {
        title: { set: 'Colin\'s Party' },
        time: { set: 4 },
        location: { set: 'Miramar, NZ' },
        attendees: {}
      }
    },
    'two branch nodes'
  )

  const G = {
    key: 'G',
    data: { title: { set: 'Party' } },
    previous: ['C', 'D', 'E']
  }
  t.deepEqual(
    new Reduce(strategy, { nodes: [A, B, C, D, E, G] }).state,
    {
      G: {
        title: { set: 'Party' },
        time: { set: 4 },
        location: { set: 'Miramar, NZ' },
        attendees: {}
      }
    },
    'two branch nodes and merge node without identity'
  )

  t.end()
})
