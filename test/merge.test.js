const test = require('tape')
const Reduce = require('../')

const Strategy = require('@tangle/strategy')
const OverwriteStrategy = require('@tangle/overwrite')

const strategy = new Strategy({
  title: OverwriteStrategy(),
  ouji: OverwriteStrategy()
})

test('reduce (merge)', t => {
  //     A   (root)
  //    / \
  //   B   C

  const A = {
    key: 'A',
    previous: null,
    data: {
      title: { set: 'my root message' },
      ouji: { set: 'hello' }
    }
  }
  const B = {
    key: 'B',
    previous: ['A'],
    data: { ouji: { set: 'mix' } }
  }
  const C = {
    key: 'C',
    previous: ['A'],
    data: {
      title: { set: 'edited message' },
      ouji: { set: 'world' }
    }
  }
  t.deepEqual(
    new Reduce(strategy, { nodes: [A, B, C] }).state,
    {
      B: {
        title: { set: 'my root message' },
        ouji: { set: 'mix' }
      },
      C: {
        title: { set: 'edited message' },
        ouji: { set: 'world' }
      }
    },
    'simple hydra'
  )

  //     A   (root)
  //    / \
  //   B   C
  //    \ /
  //     D
  const D = {
    key: 'D',
    previous: ['B', 'C'],
    data: {
      title: { set: 'edited and merged!' },
      ouji: { set: 'hello mix (world)' }
    }
  }
  // this is a valid merge because it resolves the conflict present between B + C
  // with the title + ouji properties

  t.deepEqual(
    new Reduce(strategy, { nodes: [A, B, C, D] }).state,
    {
      D: {
        title: { set: 'edited and merged!' },
        ouji: { set: 'hello mix (world)' }
      }
    },
    'merge with conflict'
  )

  t.end()
})

// TODO
test('reduce (invalid merge)', t => {
  //     A   (root)
  //    / \
  //   B   C
  //    \ /
  //     Dud  << an invalid merge message

  const A = {
    key: 'A',
    previous: null,
    data: {
      title: { set: 'my root message' },
      ouji: { set: 'hello' }
    }
  }
  const B = {
    key: 'B',
    previous: ['A'],
    data: { ouji: { set: 'mix' } }
  }
  const C = {
    key: 'C',
    previous: ['A'],
    data: {
      title: { set: 'edited message' },
      ouji: { set: 'world!' }
    }
  }
  const Dud = {
    key: 'D',
    previous: ['B', 'C'],
    data: { title: { set: 'edited and merged!' } }
    // ouji: identity()}
  }
  // this is an invalid merge message because Dud fails to resolve conflict between B + C
  // on the 'ouji' property

  t.deepEqual(
    new Reduce(strategy, { nodes: [A, B, C, Dud] }).state,
    {
      B: {
        title: { set: 'my root message' },
        ouji: { set: 'mix' }
      },
      C: {
        title: { set: 'edited message' },
        ouji: { set: 'world!' }
      }
    },
    'invalid merge message (merge message is ignored)'
  )

  t.end()
})

// TODO
test('reduce (automerge)', t => {
  //     A   (root)
  //    / \
  //   B   C
  //   |   |
  //   |   D
  //    \ /
  //     E

  const A = {
    key: 'A',
    previous: null,
    data: {
      title: { set: 'my root message' },
      ouji: { set: 'hello' }
    }
  }
  const B = {
    key: 'B',
    previous: ['A'],
    data: {
      title: { set: 'one two' },
      ouji: { set: 'mix' }
    }
  }
  const C = {
    key: 'C',
    previous: ['A'],
    data: {
      title: { set: 'one' },
      ouji: { set: 'world' }
    }
  }
  const D = {
    key: 'D',
    previous: ['C'],
    data: { title: { set: 'one two' } }
  }
  const E = {
    key: 'E',
    previous: ['B', 'D'],
    data: { ouji: { set: 'hello mix-world!' } }
  }
  // this is an valid because:
  // - 'title' transformations pre-merge are identical, so can automerge
  // - 'ouji' transformations are in conflict, but a resolution is declared

  t.deepEqual(
    new Reduce(strategy, { nodes: [A, B, C, D, E] }).state,
    {
      E: {
        title: { set: 'one two' },
        ouji: { set: 'hello mix-world!' }
      }
    },
    'automerge of identical heads + correct conflict resolution'
  )

  t.end()
})
