const test = require('tape')
const Reduce = require('../')

const Strategy = require('@tangle/strategy')
const Overwrite = require('@tangle/overwrite')
// const SimpleSet = require('@tangle/simple-set')

const strategy = new Strategy({
  title: Overwrite()
})

// TODO consider :
// - addition of semi-connected nodes
// - addition of node which connects in semi-connected node

test('addNode - adding to tip, adding alt root', t => {
  //   A   (root)
  //   |
  //   B

  const A = {
    key: 'A',
    data: { author: '@mix' },
    previous: null
  }
  const B = {
    key: 'B',
    data: {
      author: '@stranger',
      title: { set: 'nice nice' }
    },
    previous: ['A']
  }

  const smallGraph = new Reduce(strategy, { nodes: [A] })
  smallGraph.addNodes([B])
  t.deepEqual(
    smallGraph.state,
    {
      B: {
        title: { set: 'nice nice' }
      }
    },

    'Can add a node'
  )

  //   A   (root)    C
  //   |
  //   B
  const C = {
    key: 'C',
    data: {
      author: '@mix',
      title: { set: 'another root' }
    },
    previous: null
  }
  smallGraph.addNodes([C])
  t.deepEqual(
    smallGraph.state,
    {
      B: {
        title: { set: 'nice nice' }
      },
      C: {
        title: { set: 'another root' }
      }
    },

    'Can add a new root node'
  )
  t.end()
})

test('addNode - adding in non causal order', t => {
  //   A   (root)
  //   |
  //   B
  //   |
  //   C

  const A = {
    key: 'A',
    data: {
      author: '@mix',
      title: { set: 'initial' }
    },
    previous: null
  }
  const B = {
    key: 'B',
    data: {
      author: '@stranger',
      title: { set: 'nice nice' }
    },
    previous: ['A']
  }
  const C = {
    key: 'C',
    data: {
      author: '@stranger',
      title: { set: 'final' }
    },
    previous: ['B']
  }

  const reduce = new Reduce(strategy)
  reduce.addNodes([A, C])

  t.deepEqual(
    reduce.state,
    {
      A: {
        title: { set: 'initial' }
      }
    },
    'Adding a disconnected node has no effect'
  )

  reduce.addNodes([B])
  t.deepEqual(
    reduce.state,
    {
      C: {
        title: { set: 'final' }
      }
    },
    'adding a node which connects disconnected nodes ends up including them'
  )

  t.end()
})
