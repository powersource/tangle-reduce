# @tangle/reduce

Takes in a collection of tangle nodes which contain operational transforms,
and reduces them into some final transform(s).

The reduce starts with root node(s) and traverses the tangle graphs,
successively concatenating the transforms in each node until the tip(s)
of the graph are reached, where we return the final state(s)


## Example Usage


```js
//     A       (root node)
//    / \                                       |
//   B   C     (two concurrent nodes)           |  causality
//    \ /                                       v
//     D       (leading tip)

const nodes = [
  {
    key: 'A',
    previous: null,
    data: {
      title: { set: 'spec gathering' },
      attendees: { mix: 1, luandro: 1 }
    }
  },
  {
    key: 'B',
    previous: ['A'],
    data: {
      attendees: { luandro: -1, cherese: 1 }
    }
  },
  {
    key: 'C',
    previous: ['A'],
    data: {
      attendees: { luandro: -1 }
    }
  },
  {
    key: 'D',
    previous: ['B', 'C'],
    data: {
      title: { set: 'Tangle Spec v1 meeting' }
    }
  },
]
```

```js
const Reduce = require('@tangle/reduce')
const Strategy = require('@tangle/strategy')

const strategy = new Strategy({
  title: require('@tangle/simple-set')(),
  attendees: require('@tangle/overwrite')()
})

const result = new Reduce(strategy, { nodes })
// => {
//   D: {
//     title: { set: 'Tangle Spec v1 meeting' },
//     attendees: { mix: 1. luandro: -2, cherese: 1 }
//   }
// }
```

Here the only key in our result is `D`, as there's only one leading tip.
If we reduced `[A, B, C]` there would be two tips `[B, C]` and an accumulated
transform state for each.

Note you could use `strategy.mapToOutput` to convert this accumulated transformation
into a 'real' state:

```js
strategy.mapToOutput(result['D'])
// => {
//   title: 'Tangle Spec v1 meeting',
//   attendees: ['cherese', 'mix']
// }
```

## API

### `new Reduce(strategy, opts) => reduce`

Instantiates a new reduce helper

`strategy` *Object* defines how to reduce nodes. Produced by `@tangle/strategy` (version >=2)

`opts` *Object* (optional) is an object which lets you to customise the reduce:
- `opts.nodes` *Array* is a collection of tangle nodes, where each expected to include:
    - `node.key` - a unique identifier for the node
    - `node.previous`  - an Array of keys for nodes which this node extended from
    - `node.data` - an object which contains the transformation which the `strategy` describes

- `opts.isValidNextStep = fn(context, node): Boolean`
    - determine whether the next node `node` should be included in the tangle
    - `context` is the context in the tangle immediately before `node`, and is an object `{ graph, tips }`
        - `graph` is an instance of `@tangle/graph`, which has a bunch of helper functions for querying the tangle
        - `tips` which described the accumulated transform right before `node` as an object `{ key, T }`
    - NOTE if a node is not valid, all nodes downstream of that node and considered invalid, and will not be included in the reduce


### `reduce.resolve() => resolvedState`

A getter which accesses the current resolved state for the

_alias: `reduce.state` (a getter)_


### `reduce.addNodes(nodes)`

Register more nodes to include in the reducing.
